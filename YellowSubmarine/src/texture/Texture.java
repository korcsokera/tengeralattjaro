package texture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;


import texture.PNGDecoder.Format;

/**
 * Represents a texture which can be applied on polygons
 */
public class Texture {

	private int id;
	
	private int width;
	private int height;
	
	 /**
    * Gets the texture width.
    * 
    */
   public int getWidth() {
       return width;
   }

   /**
    * Sets the texture width.
    *
    */
   public void setWidth(int width) {
       if (width > 0) {
           this.width = width;
       }
   }

   /**
    * Gets the texture height.
    *
    * */
   public int getHeight() {
       return height;
   }

   /**
    * Sets the texture height.
    *    
    */
   public void setHeight(int height) {
       if (height > 0) {
           this.height = height;
       }
   }
	
	/**
	 * Create a texture by loading an image file
	 * @param filename Path of the image file for the texture
	 */
	public Texture(String filename) {
		try {
			loadTexture(filename);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Bind the texture
	 */
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, id);
	}
	
	
	/**
	 * Load texture from image file
	 * @param fileName Path of the image file for the texture
	 * @throws Exception
	 */
	private void loadTexture(String fileName) throws Exception {
		// Load Texture file
		File initialFile = new File(fileName);
		InputStream targetStream = new FileInputStream(initialFile);;
		
		PNGDecoder decoder = new PNGDecoder(targetStream);

		width = decoder.getWidth();;
		height = decoder.getHeight();;
		
		// Load texture contents into a byte buffer
		ByteBuffer buf = ByteBuffer.allocateDirect(4 * width * height);
		decoder.decode(buf, decoder.getWidth() * 4, Format.RGBA);
		buf.flip();

		// Create a new OpenGL texture
		id = glGenTextures();

		// Bind the texture
		glBindTexture(GL_TEXTURE_2D, id);
		
		// Tell OpenGL how to unpack the RGBA bytes. Each component is 1 byte
		// size
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		// Upload the texture data
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, decoder.getWidth(), decoder.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
		
		// Generate Mip Map
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	
	
	
}
