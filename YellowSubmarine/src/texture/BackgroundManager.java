package texture;

/**
 * BackgroundManager class
 * This class sets the color of sky, ocean and ocean bottom
 **/

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glColor3f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glPopMatrix;

import engine.Model;

public final class BackgroundManager {
	
	private static Texture skyTexture;
	private static Texture oceanTexture;
	private static Texture sandTexture;
	
	private static Model skyModel;
	private static Model oceanModel;
	private static Model sandModel;
	
	/**
	 * Loads and applies the background textures
	 * @param width Width of the background (usually the screen width)
	 * @param height Height of the background (usually the screen height)
	 * @param sky Name of the file in "textures" folder which contains sky texture (png image)
	 * @param ocean Name of the file in "textures" folder which contains ocean texture (png image)
	 * @param sand Name of the file in "textures" folder which contains sand texture (png image)
	 */
	public static void init(int width, int height, String sky, String ocean, String sand) {
		float[] texCoords = new float[] {
				0, 0,
				1, 0,
				1, 1,
				0, 1
			};
		
		float[] skyVertices = new float[] {
			0, height,
			width, height,
			width, 650,
			0, 650 
		};
			
		skyModel = new Model(skyVertices, texCoords);
		skyTexture = new Texture("./textures/" + sky + ".png");
			
		float[] oceanVertices = new float[] {
			0, 650,
			width, 650,
			width, 70,
			0, 70
		};
			
		oceanModel = new Model(oceanVertices, texCoords);		
		oceanTexture = new Texture("./textures/" + ocean + ".png");
				
		float[] sandVertices = new float[] {
			0, 70,
			width, 70,
			width, 0,
			0, 0
		};
				
		sandModel = new Model(sandVertices, texCoords);
		sandTexture = new Texture("./textures/" + sand + ".png");
	}
	
	/**
	 * Draws the background
	 */
	public static void drawBackground() {	
		glColor3f(255, 255, 255);
		
		glEnable(GL_TEXTURE_2D);
		
		skyTexture.bind();
		skyModel.render();
		
		oceanTexture.bind();
		oceanModel.render();
		
		sandTexture.bind();
		sandModel.render();
		
		glDisable(GL_TEXTURE_2D);
			
		glPopMatrix();
	}
	
	/**
	 * Loads another ocean texture
	 * @param name Name of the file in "textures" folder which contains ocean texture (png image)
	 */
	public static void reloadOcean(String name) {
		oceanTexture = new Texture("./textures/" + name + ".png");
	}
}
