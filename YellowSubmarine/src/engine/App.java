package engine;

/**
 * Main class
 * It is responsible for initializing the Window and creating the game objects
 **/

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SPACE;
import static org.lwjgl.glfw.GLFW.*;

import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetKey;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

import engine.Submarine.Team;
import infoline.InformationLine;
import infoline.UIText;
import texture.BackgroundManager;

import static org.lwjgl.opengl.GL11.*;

public class App {

	private GLFWErrorCallback errorCallback;
	private GLFWKeyCallback keyCallback;

	private long window;	

	private UIText level;
	private UIText life;
	private UIText progress;
	private UIText score;	
	private Submarine player;
	private ArrayList<Submarine> enemies;
	private Camera camera = new Camera();
	
	private static final int WIDTH = 1024;
	private static final int HEIGHT = 768;
	
	private Boolean isShot = false;;
	private Boolean crash = false;
	
	private InformationLine infoLine;
	
	private int spaceCooldown = 0;

	/**
	 * Runs the game. Calls init(), than loop()
	 */
	public void run() {
		System.out.println("LWJGL version: " + Version.getVersion());
		try {
			init();
			loop();

			glfwDestroyWindow(window);
			keyCallback.free();
		} finally {
			glfwTerminate();
			errorCallback.free();
		}
	}

	/**
	 * Initial setup: drawing windows, setting up keycallbacks, camera, LevelManager, BackgroundManager, InfoText
	 */
	private void init() {
		//Setup an error callback. The default implementation will print the error message in System.err.
		glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));

		//Initialize GLFW
		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		//optional, the current window hints are already the default
		glfwDefaultWindowHints(); 
		//the window will stay hidden after creation
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		//the window will be resizable
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);	

		//creating the window
		window = glfwCreateWindow(WIDTH, HEIGHT, "Yellow Submarine v0.1", NULL, NULL);																													
		if (window == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		//Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
					glfwSetWindowShouldClose(window, true);
				}
			}
		});

		//get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		//centering the window
		glfwSetWindowPos(window, (vidmode.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 2);
		
		//make the OpenGL context current
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);
		
		//make the window visible
		glfwShowWindow(window);
		GL.createCapabilities();
		
		glEnable(GL_BLEND);
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		//set camera
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		
		camera.setCamera(WIDTH, HEIGHT);
		
		//starting level manager
		LevelManager.init();
		
		//initializing background textures
		BackgroundManager.init(WIDTH, HEIGHT, "sky", "ocean0", "sand");
		
		//creating submarines
		player = new Submarine(new Point(500.0f, 300.0f), new Color(1.0f, 1.0f, 0.0f), Team.FRIENDLY);
		enemies = new ArrayList<Submarine>();
		fillEnemies(enemies, 10);
		
		infoLine = new InformationLine(0, 1, 3, 0, 20);
		
//		score = new UIText(0, 0, "Score 0");
//		level = new UIText(250, 0, "Level 1");
//		life = new UIText(500, 0, "Life: 3");
//		progress = new UIText(1000, 0, "0/20 km");

	}

	/**
	 * Create the enemy submarine by the given number. The position of each enemy is a random number
	 * @param enemies  the list of all enemies
	 * @param count    number of enemies
	 */
	private void fillEnemies(ArrayList<Submarine> enemies, int count) {
		
		Random rand = new Random();
		
		for(int i = 0; i < count; i++) {
			Submarine enemy = new Submarine(new Point(WIDTH + rand.nextFloat() * 1000, i*46 + 100), new Color(1.0f, 0.0f, 0.0f), Team.ENEMY);
			enemy.setSpeed(rand.nextFloat() * 4 * LevelManager.getEnemySpeed());
			enemies.add(enemy);
		}
	}	
	
	/**
	 * Game loop
	 */
	private void loop() {

		GL.createCapabilities();
		// glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

		while (!glfwWindowShouldClose(window)) {		
			//handling the key pressings
			keyPush();					
			
			//drawing background textures
			BackgroundManager.drawBackground();				
			
			player.draw();
			
			
			/**
			 * If an enemy submarine leaves a screen, it gets a new position as a start position
			 */
			
			Random rand = new Random();
			for(int i = 0; i < enemies.size(); i++) {
				enemies.get(i).update();
				if (enemies.get(i).onScreen(WIDTH, HEIGHT)) {
					enemies.get(i).setPosition(new Point(WIDTH + rand.nextFloat() * 1000, i*46 + 100)); 
					enemies.get(i).setSpeed(rand.nextFloat() * 4 * LevelManager.getEnemySpeed());
				}
				enemies.get(i).draw();		
			}
			
			/**
			 * Checking if an enemy crash into player.
			 * Score -10, and enemy is removed.
			 */
			
			for(int i = 0; i < enemies.size(); i++) {
				int currentScore = infoLine.getScore();
				crash = player.isCrashed(enemies.get(i).getBoundingBox());
				if (crash)
				{
					enemies.get(i).setPosition(new Point(WIDTH + 500, HEIGHT + 400));
					System.out.println("Crashed! enemy " + i  +" - app.java");
					infoLine.setScore(currentScore - 10); 
				}
				crash = false;
			}
			
			ListIterator<Projectile> iterator = player.getProjectiles().listIterator();
			while (iterator.hasNext()) {
				Projectile item = iterator.next();
				item.update();
				if (item.onScreen(WIDTH, HEIGHT))
					item.draw();
				
				else
					iterator.remove();
				int i = 0;
				int currentScore = infoLine.getScore();
								
				
				/**
				 * Examine every enemy and if one is shot, then it disappears, gets a new position as a start position, and points increase
				 * Points are currently displayed on console
				 */
				while (i < enemies.size()) {
					isShot = enemies.get(i).isShot(item.getBoundingBox());
					if (isShot) {
						enemies.get(i).setPosition(new Point(WIDTH + 500, HEIGHT + 400));
						System.out.println("enemy " + i + " is shot");
						infoLine.setScore(currentScore + 10); 
						iterator.remove();
					}
					
					isShot = false;
					i++;
				}
			}
			
			if (spaceCooldown > 0)
				spaceCooldown--;
			
			glfwSwapBuffers(window);
			glfwPollEvents();
			
			System.out.println("Score: " + infoLine.getScore());			
			//level.setText("Level " + infoLine.getLevel());
			//score.setText("Score " + infoLine.getScore());
			//life.setText("Life " + infoLine.getLife());
			//progress.setText(infoLine.getProgress() + "/" + infoLine.getDistance());
			
			//score.draw();
			//	level.draw();
			//life.draw();
			//progress.draw();
			
			if (LevelManager.update(infoLine.getScore())) {
				System.out.println("Level " + LevelManager.getCurrent());
			}
		}
	}
	
	/**
	 * Checks is dedicated keys are pressed and performs actions accordingly
	 */
	private void keyPush() {
		float speed = LevelManager.getPlayerSpeed() * 5.0f;
		
		//moving the tube down
		if (isKeyPressed(GLFW_KEY_DOWN)) {			
			if (player.getTubeAngle() >= -85)			
				player.setTubeAngle(player.getTubeAngle() - 3);
			
		}
		//moving the tube up
		if (isKeyPressed(GLFW_KEY_UP)) {				
				if (player.getTubeAngle() <= 85)
					player.setTubeAngle(player.getTubeAngle() + 3);
		}
		//moving the submarine up
		if (isKeyPressed(GLFW_KEY_W)) {
			
			//top of the ocean
			if(player.getPosition().getY() >= 650.0f)
				player.getPosition().setY(650.0f);
			else 
				player.getPosition().setY(player.getPosition().getY() + speed);
		}
		//moving the submarine down
		if (isKeyPressed(GLFW_KEY_S)) {
			//bottom of the ocean
			if(player.getPosition().getY() <= 90.0f)		
				player.getPosition().setY(90.0f);
			else 
				player.getPosition().setY(player.getPosition().getY() - speed);
		}
		//moving the submarine forward
		if (isKeyPressed(GLFW_KEY_D)) {			
			//right border of the screen
			if(player.getPosition().getX() >= 980.0f)		
				player.getPosition().setX(980.0f);
			else
				player.getPosition().setX(player.getPosition().getX() + speed);
		} 
		//moving the submarine backward
		if (isKeyPressed(GLFW_KEY_A)) {
			//left border of the screen
			if(player.getPosition().getX() <= 44.0f)
				player.getPosition().setX(44.0f);
			else 
				player.getPosition().setX(player.getPosition().getX() - speed);
		}
		//shooting
		if (isKeyPressed(GLFW_KEY_SPACE)) {			
			if (spaceCooldown == 0) {
				player.shoot();
				spaceCooldown = LevelManager.getShootingSpeed();
			}
		}
		else
			spaceCooldown = 0;
	}	

	/**
	 * Helper method registering keypresses
	 * @param keyCode The keycode of the key we want to check
	 * @return true, if the key is pressed, false if not pressed
	 */
	public boolean isKeyPressed(int keyCode) {
        return glfwGetKey(window, keyCode) == GLFW_PRESS;
    }

	/**
	 * Starting the game
	 * @param args
	 */
	public static void main(String[] args) {		
		new App().run();		
	}
}

