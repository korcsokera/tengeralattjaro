package engine;

/**
 * Level class
 * Represents a game level
 **/

public class Level {

	private String name;
	private float playerSpeed;
	private int shootingSpeed;
	private float enemySpeed;
	private long scoreNeeded;
	
	public Level(String name, float playerSpeed, int shootingSpeed, float enemySpeed, int scoreNeeded) {
		this.playerSpeed = playerSpeed;
		this.shootingSpeed = shootingSpeed;
		this.enemySpeed = enemySpeed;
		this.name = name;
		this.scoreNeeded = scoreNeeded;
	}
	
	public Level(float playerSpeed, int shootingSpeed, float enemySpeed, int scoreNeeded) {
		this("", playerSpeed, shootingSpeed, enemySpeed, scoreNeeded);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public float getPlayerSpeed() {
		return playerSpeed;
	}
	
	public void setPlayerSpeed(float playerSpeed) {
		this.playerSpeed = playerSpeed;
	}
	
	public int getShootingSpeed() {
		return shootingSpeed;
	}
	
	public void setShootingSpeed(int shootingSpeed) {
		this.shootingSpeed = shootingSpeed;
	}
	
	public float getEnemySpeed() {
		return enemySpeed;
	}
	
	public void setEnemySpeed(float enemySpeed) {
		this.enemySpeed = enemySpeed;
	}

	public long getScoreNeeded() {
		return scoreNeeded;
	}

	public void setScoreNeeded(int scoreNeeded) {
		this.scoreNeeded = scoreNeeded;
	}
}
