package engine;

/**
 * Circle class
 * Represents a circle object which is needed for creating the tube 
 **/

public class Circle {

	private Point center;
	private float r;
	
	public Circle(Point center, float r) {
		this.center = center;
		this.r = r;
	}
	
	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public float getR() {
		return r;
	}
	
	public void setR(float r) {
		this.r = r;
	}
	
	//get the ith point of circle
	public Point circlePoint(int i, float submarineX, float submarineY) {
		Point p = new Point(0, 0);
		p.setX((float) (submarineX + r * Math.cos(Math.toRadians(i))));
		p.setY((float) (submarineY + r * Math.sin(Math.toRadians(i))));
		return p;
	}	
	
	public Point propellerPoint(int i) {
		Point p = new Point(0, 0);
		p.setX((float) (center.getX() + r * Math.sin(Math.toRadians(1) * (i / (2 * r * 3.14f))) * r));
		p.setY((float) (center.getY() + Math.cos(Math.toRadians(1) * (i / (2 * r * 3.14f))) * r));
		return p;
	}
}
