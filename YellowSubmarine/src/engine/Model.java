package engine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

/**
 * Represents a polygon with texture.
 */
public class Model {

	private int drawCount;
	
	private int vId;
	private int tId;
	
	/**
	 * Create a polygon only with its angles
	 * @param vertices Angles of the polygon
	 */
	public Model(float[] vertices) {
		drawCount = vertices.length / 2;
		
		vId = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vId);
		glBufferData(GL_ARRAY_BUFFER, createBuffer(vertices), GL_STATIC_DRAW);
		
		tId = 0;
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	/**
	 * Create a polygon only angles and texture
	 * @param vertices Angles of the polygon
	 * @param texCoords Texture coordinates
	 */
	public Model(float[] vertices, float[] texCoords) {
		this(vertices);
		
		tId = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, tId);
		glBufferData(GL_ARRAY_BUFFER, createBuffer(texCoords), GL_STATIC_DRAW);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	
	/**
	 * Draw the polygon
	 */
	public void render() {
		glEnableClientState(GL_VERTEX_ARRAY);
		if (tId != 0)
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		
		glBindBuffer(GL_ARRAY_BUFFER, vId);
		glVertexPointer(2, GL_FLOAT, 0, 0);
		
		if (tId != 0) {
			glBindBuffer(GL_ARRAY_BUFFER, tId);
			glTexCoordPointer(2, GL_FLOAT, 0, 0);
		}
		
		glDrawArrays(GL_QUADS, 0, drawCount);
		
		glBindBuffer(GL_ARRAY_BUFFER, 0);		
		glDisableClientState(GL_VERTEX_ARRAY);
		if (tId != 0)
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	
	/**
	 * Create a float buffer and fill it
	 * @param data Values for the buffer
	 * @return The created and filled FloatBuffer
	 */
	private FloatBuffer createBuffer(float[] data) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		
		return buffer;
	}
}
