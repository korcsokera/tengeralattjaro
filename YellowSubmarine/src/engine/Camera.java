package engine;

/**
 * Camera class
 * This class sets the 2D camera
 **/

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

public class Camera {
	public void setCamera(int width, int height) {
		//clear screen
		glClear(GL_COLOR_BUFFER_BIT);
		
		//modify the projectionm matrix
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, 0, height, -1, 1);
		
		//modify the modelview matrix
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}	
}
