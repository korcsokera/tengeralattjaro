package engine;

/*
 * Color class. 
 * This class can set and get the color by red, green and blue value
 */

public class Color {

	private float r;
	private float g;
	private float b;
	
	/**
	 * Class constructor, set the init value of color by RGB components
	 * @param r  red component of RGB
	 * @param g  green component of RGB
	 * @param b  blue component of RGB
	 */
	public Color(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getG() {
		return g;
	}

	public void setG(float g) {
		this.g = g;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}
}
