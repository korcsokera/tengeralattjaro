package engine;

import javax.crypto.spec.PSource.PSpecified;

import org.joml.Vector2f;

/**
 * Projectile class
 * Represents a projectile which is fired by a submarine
**/

import org.lwjgl.opengl.GL11;

public class Projectile {

	private Point start;
	private Point position;
	
	private float speed;	
	private float angle;
	private float size;
	
	private Submarine shooter;
	
	private Color color;
	
	private BoundingBox boundingBox;
	
	/**
	 * Class constructor, it sets the start position, speed, angle and size of the Projectile, and the submarine 
	 * @param start  		starting point of the projectile
	 * @param speed  		speed of the projectile
	 * @param angle  		current angle of the tube
	 * @param size   		size of the projectile
	 * @param shooter	 	the submarine which is shooting (either enemy or friendly)
	 */
	public Projectile(Point start, float speed, int angle, float size, Submarine shooter) {
		this.start = new Point(start.getX(), start.getY());
		this.position = this.start;
		this.speed = speed;
		this.angle = angle;
		this.size = size;
		this.shooter = shooter;
	}
	
	public Projectile(Point start, float speed, int angle, Submarine shooter) {
		this(start, speed, angle, 2f, shooter);
	}
	
	public Projectile(Point start, float speed, int angle) {
		this(start, speed, angle, null);
	}

	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public Submarine getShooter() {
		return shooter;
	}
	
	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}
	
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}
	
	
	
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(BoundingBox boundingBox) {
		this.boundingBox = boundingBox;
	}

	public Boolean onScreen(int screenX, int screenY) {
		if (position.getX() <= screenX && position.getX() >= 0 && position.getY() <= screenY && position.getY() >= 0)
			return true;
		return false;
	}

	public void update() {
		float a = (float) (Math.sin(Math.toRadians(angle)) * speed);
		float b = (float) (Math.cos(Math.toRadians(angle)) * speed);
		position.setX(position.getX() + b);
		position.setY(position.getY() + a);
	}

	//drawing an actual projectile
	public void draw() {
		GL11.glColor3f(1.0f, 1.0f, 1.0f);
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glVertex2f(position.getX() - size, position.getY() - size);
			GL11.glVertex2f(position.getX() - size, position.getY() + size);
			GL11.glVertex2f(position.getX() + size, position.getY() + size);
			GL11.glVertex2f(position.getX() + size, position.getY() - size);
		GL11.glEnd();
	
		GL11.glPopMatrix();
		
		boundingBox = new BoundingBox(new Vector2f(position.getX() - size, position.getY() - size),
										new Vector2f(position.getX() + size, position.getY() + size));
		boundingBox.setCorners();
	}
}
