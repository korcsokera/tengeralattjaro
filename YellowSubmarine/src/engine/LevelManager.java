package engine;

/**
 * LevelManager class
 * This class is responsible for managing the levels and setting the difficulty
 **/

import java.util.ArrayList;
import java.util.List;

import texture.BackgroundManager;

public final class LevelManager {

	private static List<Level> levels = new ArrayList<Level>();
	
	private static int current;
	
	/**
	 * Initial setting of levels' data
	 */
	public static void init() {
		levels.add(new Level("Level_0_name", 1f, 10, 0.5f, 0));
		levels.add(new Level("Level_1_name", 0.9f, 9, 0.6f, 80));
		levels.add(new Level("Level_2_name", 0.8f, 8, 0.7f, 200));
		
		current = 0;
	}
	
	/**
	 * Checks if the current score is enough to step to the next level and if yes, calls a method which loads next level.
	 * @param score Current score of the player
	 * @return true, if the score is enough, else false
	 */
	public static boolean update(int score) {
		if ((current + 1 < levels.size()) && (levels.get(current + 1).getScoreNeeded() <= score)) {
			System.out.println(current + " " + levels.get(current + 1).getScoreNeeded() + " " + score + " " + levels.get(current).getName() + " " + levels.get(current + 1).getName());
			loadNextLevel();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns a level by index
	 * @param index Index of the level
	 * @return The level on index place in levels
	 */
	public static Level get(int index) {
		return levels.get(index);
	}
	
	public static int getCurrent() {
		return current;
	}
	
	public static float getPlayerSpeed() {
		return levels.get(current).getPlayerSpeed();
	}
	
	public static int getShootingSpeed() {
		return levels.get(current).getShootingSpeed();
	}
	
	public static float getEnemySpeed() {
		return levels.get(current).getEnemySpeed();
	}
	
	public static float getScoreNeeded() {
		return levels.get(current).getScoreNeeded();
	}
	
	/**
	 * Increase the level number by 1.
	 */
	public static void increase() {
		if (levels.size() > current + 1)
			current++;
	}
	
	/**
	 * Decrease the level number by 1.
	 */
	public static void decrease() {
		if (current > 0)
			current--;
	}
	
	/**
	 * Calls BackgroundManager to set the approriate background
	 */
	public static void loadBackground() {
		BackgroundManager.reloadOcean("ocean" + current);
	}
	
	/**
	 * Loads the next level
	 */
	public static void loadNextLevel() {
		increase();
		loadBackground();
	}
}
