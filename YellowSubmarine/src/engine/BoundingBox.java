package engine;

import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL40;
import org.lwjglx.BufferUtils;

public class BoundingBox {

	private Vector2f bbMin;
	private Vector2f bbMax;
	private Vector2f[] bbPoints;
	
	private float bbHalfW;
	private float bbHalfH;
	
	private Matrix4f transformationMatrix;
		
	/**
	 * Class constructor. Initialize the data of the bounding box
	 */
	public BoundingBox(Vector2f min, Vector2f max) {
		
		//initialize the variables
		bbHalfH = 0;
		bbHalfW = 0;
		
		bbMin = new Vector2f(min.x, min.y);
		bbMax = new Vector2f(max.x, max.y);
		
		bbPoints = new Vector2f[4];
		for(int i = 0; i < bbPoints.length; i++) {
			bbPoints[i] = new Vector2f();
		}
		
		transformationMatrix = new Matrix4f();
		
		setCorners();
		searchMinMax();
		
		//calculate the center
		bbHalfH = (bbMax.y - bbMin.y) / 2.0f;
		bbHalfW = (bbMax.x - bbMin.x) / 2.0f;
		
	}
	
	/**
	 * Search the minimum and maximum values
	 */
	public void searchMinMax()
	{
		Vector2f min = new Vector2f(bbPoints[0].x, bbPoints[0].y);
		Vector2f max = new Vector2f(bbPoints[0].x, bbPoints[0].y);
		
		for(int i = 0; i < 4; i++)
		{
			if (bbPoints[i].x < min.x) {
				min.x = bbPoints[i].x;
			}
			if (bbPoints[i].y < min.y) {
				min.y = bbPoints[i].y;
			}

			if (bbPoints[i].x > max.x) {
				max.x = bbPoints[i].x;
			}
			if (bbPoints[i].y > max.y) {
				max.y = bbPoints[i].y;
			}
		}
		
		bbMin.x = min.x;
		bbMin.y = min.y;
		
		bbMax.x = max.x;
		bbMax.y = max.y;
	}
	
	/**
	 * Set the bounding box corner coordinates. This coordinates based on minimum and maximum points
	 */
	public void setCorners() {
		
		//left bottom
		bbPoints[0].x = bbMin.x;
		bbPoints[0].y = bbMin.y;
		
		//right bottom
		bbPoints[1].x = bbMax.x;
		bbPoints[1].y = bbMin.y;
		
		//right top
		bbPoints[2].x = bbMax.x;
		bbPoints[2].y = bbMax.y;
		
		//left top
		bbPoints[3].x = bbMin.x;
		bbPoints[3].y = bbMax.y;
	}
	
	/**
	 * Set the bounding box points and the center
	 * @param min
	 * @param max
	 */
	
	public void setBBPoints(Vector2f min, Vector2f max) {
		bbMin.set(min.x, min.y);
		bbMax.set(max.x, max.y);
		
		setCorners();
		searchMinMax();
		
		bbHalfH = (bbMax.y - bbMin.y) / 2.0f;
		bbHalfW = (bbMax.x - bbMin.x) / 2.0f;	
	}
	
	public Vector2f getMax() {
		return bbMax;
	}
	
	public Vector2f getMin() {
		return bbMin;
	}
	
	public Vector2f[] getPoints() {
		return bbPoints;
	}
	
	/**
	 * Check the boxes overlapping
	 * @param itemPoint Represent an item's x and y coords
	 * @return
	 */
		
	public boolean checkCollision(BoundingBox box2) {
		
		
		if(bbMax.x < box2.getMin().x || bbMin.x > box2.getMax().x) {
			return false;
		}
		
		if(bbMax.y < box2.getMin().y || bbMin.y > box2.getMax().y ) {
			return false;
		}
		
		return true;
	}
	
	
	
	

}
