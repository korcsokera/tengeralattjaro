package engine;

/**
 * Submarine class
 * Represents a submarine with its properties
 **/

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

import texture.Texture;

/*
 * Submarine class
 * This class contains the submarine, and its data: position, color. 
 * It also represent the player's submarine and enemy's
 * Submarine can be displayed by draw method.
 */

public class Submarine {
	
	public enum Team {
		FRIENDLY,
		ENEMY
	}
	
	private Point position;
	private Color color;
		
	private int tubeAngle;
	private Point shootingPoint;
	
	private float speed;
	
	private List<Projectile> projectiles;
	
	private Team team;
	
	private Texture submarineTexture;
	private Texture tubeTexture;
	
	private BoundingBox boundingBox;
	private BoundingBox bbPlayer;
	
	/**
	 * Class constructor, set the init values
	 * @param position
	 * @param color
	 * @param team	the player or the enemy
	 */
	
	public Submarine(Point position, Color color, Team team) {
		this.position = position;		
		this.color = color;
		this.team = team;
		projectiles = new ArrayList<>();
		
		if (this.team == Team.FRIENDLY) {
			this.submarineTexture = new Texture("./textures/friendlysub.png");
		}
		else {
			this.submarineTexture = new Texture("./textures/enemysub.png");
			
		}
		this.tubeTexture = new Texture("./textures/tube.png");
		
		
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getTubeAngle() {
		return tubeAngle;
	}

	public void setTubeAngle(int tubeAngle) {
		this.tubeAngle = tubeAngle;
	}
	
	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	
	
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	public void setBoundingBox(BoundingBox boundingBox) {
		this.boundingBox = boundingBox;
	}

	public List<Projectile> getProjectiles() {
		return projectiles;
	}

	public void shoot() {
		projectiles.add(new Projectile(this.shootingPoint, 2f, this.tubeAngle, this));
	}
	
	public void update() {
		setPosition(new Point(position.getX() - speed, position.getY()));
	}
	
	/**
	 * Separately draw the tube of then submarine and itself
	 */
	
	public void draw() {
		this.drawTube();
		this.drawSubmarine();
	}

	/**
	 * Displays the submarines. Not final method.
	 */
	public void drawSubmarine() {
		glPushMatrix();

		float[] vertices = new float[] {
			position.getX() - 50, position.getY() - 15,
			position.getX() + 50, position.getY() - 15,
			position.getX() + 50, position.getY() + 15,
			position.getX() - 50, position.getY() + 15
		};
			
		float[] texCoords = new float[] {
				0, 1,
				1, 1,
				1, 0,
				0, 0
		};
			
		Model model = new Model(vertices, texCoords);
			
		glEnable(GL_TEXTURE_2D);
			
		this.submarineTexture.bind();
		model.render();
			
		glDisable(GL_TEXTURE_2D);
			
		glPopMatrix();
		
		boundingBox = new BoundingBox(new Vector2f(position.getX() - 50, position.getY() - 15), 
					new Vector2f(position.getX() + 50, position.getY() +15));
		boundingBox.setCorners();
		
	}
	
	public void drawTube() {
		glPushMatrix();
		glColor3f(255, 255, 255);
		
		//center of the tube
		Point tubeCenter;
		if (this.team == Team.ENEMY) {
			tubeCenter = new Point(position.getX() - 15, position.getY());
		}
		else {
			tubeCenter = new Point(position.getX() + 15, position.getY());
		}
		
		//Set inner and outer coordinates of the circles. Points of the tube are moving on these circles.		
		float innerR;
		float outerR;
		if (this.team == Team.ENEMY) {
			innerR = -10f;
			outerR = -40f;
		}
		else {
			innerR = 10f;
			outerR = 40f;
		}
		
		//set an inner circle to calculate the insider points
		Circle innerCircle = new Circle(tubeCenter, innerR);  
		
		//set an outer circle to calculate the outsider points
		Circle outerCircle = new Circle(tubeCenter, outerR);  
		
		//pontok koordin�t�inak sz�m�t�sa
		shootingPoint = outerCircle.circlePoint(tubeAngle, innerCircle.getCenter().getX(), innerCircle.getCenter().getY());
		Point tubePoint1 = innerCircle.circlePoint(tubeAngle + 20, outerCircle.getCenter().getX(), outerCircle.getCenter().getY());
		Point tubePoint2 = innerCircle.circlePoint(tubeAngle - 20, outerCircle.getCenter().getX(), outerCircle.getCenter().getY());
		Point tubePoint3 = outerCircle.circlePoint(tubeAngle - 5, innerCircle.getCenter().getX(), innerCircle.getCenter().getY());
		Point tubePoint4 = outerCircle.circlePoint(tubeAngle + 5, innerCircle.getCenter().getX(), innerCircle.getCenter().getY());
		
		float[] vertices = new float[] {
			tubePoint1.getX(), tubePoint1.getY(),
			tubePoint2.getX(), tubePoint2.getY(),
			tubePoint3.getX(), tubePoint3.getY(),
			tubePoint4.getX(), tubePoint4.getY()
		};
		
		float[] texCoords = new float[] {
				0, 1,
				1, 1,
				1, 0,
				0, 0
		};
		
		Model model = new Model(vertices, texCoords);
		
		glEnable(GL_TEXTURE_2D);
		
		this.tubeTexture.bind();
		model.render();
		
		glDisable(GL_TEXTURE_2D);
		
		glPopMatrix();
	}
	
	/**
	 * Examine if the submarine is inside or outside the screen
	 * @param width	  the width of the window
	 * @param height  the height of the window
	 * @return
	 */
	
	public Boolean onScreen(int width, int height) {
		
		if (position.getX() + 40 <= 0)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * Examine if the bullet reaches the submarine. If the bullet and submarine's bounding box is equal, then submarine is shot.
	 * @param bulletPosition 
	 * @return
	 */
	
	public Boolean isShot(BoundingBox bulletBox) {
		
		/*boolean collision = boundingBox.checkCollison(bulletPosition);*/
		Boolean shooted = boundingBox.checkCollision(bulletBox);
		
		return shooted;
	}
	
	public Boolean isCrashed(BoundingBox enemyBox)
	{
		Boolean crashed = boundingBox.checkCollision(enemyBox);
		
		return crashed;
	}
	
}
