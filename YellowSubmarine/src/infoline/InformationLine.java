package infoline;

/**
 * InformationLine class
 * It shows informations about current progress of the game, like score, level, health
 * Currently it is displayed on Java console
 **/

public class InformationLine {

	private int score;
	private int level;
	private int life;
	private int progress;
	private int distance;
	
	
	/**
	 * Class constructor, set the init values
	 * @param score      score increase if az enemy is shooted
	 * @param level
	 * @param life       init values is 3. Life decreases if an enemy crashes the player.
	 * @param progress   
	 * @param distance
	 */
	public InformationLine(int score, int level, int life, int progress, int distance) {
		this.score = score;
		this.level = level;
		this.life = life;
		this.progress = progress;
		this.distance = distance;
	}

	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
		if (score % 100 == 0) {
			level++;
		}
	}

	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	/**
	 * If life decreases to 0, the player dies. The game restarts
	 * @return
	 */
	public Boolean isDead() {
		if(life == 0) {
			return true;
		}
		return false;
	}

}
