package infoline;

import java.awt.Color;

/**
 * UIText class
 * It draws text on the game UI (it's not fully implemented yet)
 * Sets the font type and size
 **/

import java.awt.Font;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

/**
 * Represents a text of the UI
 */
public class UIText {

	private TrueTypeFont font;
	private Font awtFont;

	private int x;
	private int y;
	private String text;
	
	/**
	 * Create a text via providing its coordinates
	 * @param x X coordinate of the screen where the text should appear
	 * @param y Y coordinate of the screen where the text should appear
	 * @param text The message of the textbox
	 */
	public UIText(int x, int y, String text) {
		awtFont = new Font("Times New Roman", Font.BOLD, 24);
		font = new TrueTypeFont(awtFont, false);
		this.x = x;
		this.y = y;
		this.text = text;
	}
	
	/**
	 * Create an empty text via providing its coordinates
	 * @param x X coordinate of the screen where the text should appear
	 * @param y Y coordinate of the screen where the text should appear
	 */
	public UIText(int x, int y) {
		this(x, y, "");
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public void setCoorditanes(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public String getText() {
		return this.text;
	}
	
	/**
	 * Draw the text
	 */
	public void draw() {
//		UnicodeFont font = new UnicodeFont(awtFont);
//		font.addAsciiGlyphs();
//		font.getEffects().add(new ColorEffect(Color.WHITE));
//
//		try {
//			font.loadGlyphs();
//		} catch(SlickException e) {
//				e.printStackTrace();
//				
//		}
		font.drawString(x, y, text);
	}
}
